package ENUM19;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int [] array = {1,2,4,6,1,0};
        move(array);
    }

    public static void move(int[] array){
        for(int i = array.length ; i > 0; i--){
            for(int j = 1 ; j < i; j++){
                if (array[j] == 0 && array[j-1] != 0){
                    int temp = array[j];
                    array[j] = array[j-1];
                    array[j-1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
